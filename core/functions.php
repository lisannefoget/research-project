<?php

function authenticated($redirect="") {
	if(isset($_SESSION['username'])) {
        return true;
	} else {
        if($redirect != "") {
            header("Location: $redirect");
        }
        return false;
    }
}

function signIn($username, $password, $db){
    $sql = "SELECT username FROM login WHERE username = ? AND password = ? ";
    $stmt = $db->prepare($sql); 
    $stmt->bind_param("ss", $username, $password);
    $stmt->execute();
    if($stmt->fetch()){
        return true;        
    } else {
        return false;        
    }
}

	// Gets post from db.
	function getPost($postid,$db) {
		if (is_numeric($postid)) {
			
			$sql = "SELECT p.*,a.* FROM `posts` AS p INNER JOIN `authors` AS a ON p.author_id = a.id WHERE p.id = ?";
			//$sql = "SELECT p.id, p.title, p.teaser, p.body, p.author_id, p.post_date,authors.fname, authors.lname FROM `posts` INNER JOIN `authors` ON posts.author_id = authors.id WHERE id = ?";

			$stmt = $db->prepare($sql);			//creating an object for the prepared statment
			$stmt->bind_param("i",$postid);		// take the data passed, and bind it
			$stmt->execute();
			
			$result = $stmt->get_result(); 		//return the data from the statment
			$row = $result->fetch_assoc();		// Get the row for the returned data
			
			$result->close();
			$stmt->close(); 

			return $row;
		}
		else {
			return null;
		}
	}

	
	function getLatestPosts($amount, $db) {
		// Make sure what was passed in is numeric
		if (is_numeric($amount)) {
			
			$sql  = "SELECT id, title, teaser, body, author_id, post_date FROM `posts` order by post_date DESC LIMIT ?";
			
			$stmt = $db->prepare($sql); 		//creating an object for the prepared statment
			$stmt->bind_param("i",$amount); 	// take the data passed, and bind it
			$stmt->execute();
			
			$result = $stmt->get_result(); 		//return the data from the statment
			
			$data = array(); // We create an array to store the array we are going to fetch.
			while ($row = $result->fetch_assoc()) {
				$data[] = $row;
			}
			
			$result->close();
			$stmt->close(); 

			return $data;
		}
		else {
			return null;
		} 
	}


	function createPost($author_id, $title, $teaser, $body, $db) {
	    $sql = "INSERT INTO posts (author_id, title, teaser, body) VALUES (?,?,?,?)";
	    $stmt = $db->prepare($sql);
	    
	    if ($stmt) {
	        $stmt->bind_param('isss',$author_id, $title, $teaser, $body );
	    }  
	    
	    $stmt->execute(); //after we execute our insert we will have access to the ID of the inserted row
	    $stmt->close();
	    
        return $db->insert_id;
	}


	function updatePost($author_id, $title, $teaser, $body, $db) {}
	

	function deletePost($post_id, $db) {}

// ---------------------------------------------------------------
	// Author functions [for now OR create a new functions file for them]
// ---------------------------------------------------------------
	function getAuthors($db) {
		$sql = "SELECT id, fname, lname FROM authors ORDER BY fname";
		
		$stmt = $db->prepare($sql);
		$stmt->execute();

		$result = $stmt->get_result();

		while($row = $result->fetch_assoc()) {
			$resultArray[] = $row;
		}
		
		$result->close();
		$stmt->close();

		return $resultArray;
	}	










?>