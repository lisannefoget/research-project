<?php

	session_start(); 

	// Database Config
	$db_host = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_database = "simpleblog";

	$db = new mysqli($db_host,$db_user,$db_pass,$db_database);

	// Check if we had any errors
 	if ($db->connect_errno > 0) {
		die('Had trouble connecting to the database [' . $db->connect_error . ']');
 	}

	// Site Settings
	$site_name = "Gumby Sample";
	$page_title = $site_name; //Sets a default for page_title

	$site_URL = "http://localhost:88/nbcc/feb11/";
?>