<?php 
    // Our Blogs Main Includes
    include_once('./core/config.php');     // Holds some vars so we only have one place to change settings/config
    include_once('./core/functions.php');  // Functions to do things with posts

    // Setup parts of the page.
    $page_title = "$site_name - Welcome";  

    include_once('./template/siteheader.php');

    include_once('./template/header.php');
    ?>

    <article class="container box style3">
        <?php 
        foreach (getLatestPosts(5,$db) as $post) {
          ?>
          <section class="post" data-postid="<?=$post["id"]?>">
            <header>
                <h2><a href="./post.php?id=<?=$post["id"]?>"><?=$post["title"]?></a></h2>
                <p><?=$post["teaser"]?></p>
            </header>
        </section>
        <?php
        }
    ?>
    </article>    

    <?php 
    include_once('./template/footer.php');
    include_once('./template/sitefooter.php'); 
    ?>